package stracture;
import java.util.Scanner;

public class Separate2 {

	public static void main(String[] args) {

		// 部材の最大長さ
		final int maxItemWidth = 4;
		// 部材の最小長さ
		final int minItemWidth = 1;

		while(true){
			int lineWidth = inputLineWidth();
			createSeparateLine(lineWidth,maxItemWidth,minItemWidth);
		}

	}

	public static int inputLineWidth(){
		System.out.println("ラインの長さを入力してください");
		//ライン長さ
		final int lineWidth = new Scanner(System.in).nextInt();
		return lineWidth;
	}

	public static void createSeparateLine(int lineWidth,int maxItemWidth,int minItemWidth) {

		// 最大のセパレート部材を格納
		final String separateItem = createSeparateItem(maxItemWidth);
		// 最小のセパレート部材を
		final String MinSeparateItem = createSeparateItem(minItemWidth);
		// 分割数を計算
		final int separateSpan = calcSeparateSpan(lineWidth, separateItem);
		// 余りの長さを表示
		final int remains = lineWidth - separateItem.length() * separateSpan;
		// セパレートを表示
		for (int i = 0; i < separateSpan; i++) {
			System.out.print(separateItem);
		}
		// 最小セパレートを表示
		if (remains > MinSeparateItem.length()) {
			System.out.print(MinSeparateItem);
		}
		System.out.println();
	}

	public static String createSeparateItem(int itemWidth) {
		String separateItem = "";
		final String leftParts = "◎";
		final String rightParts = "◎";
		final String centerParts = "ー";
		separateItem = leftParts;
		for (int i = 1; i <= itemWidth; i++) {
			separateItem += centerParts;
		}
		separateItem += rightParts;
		return separateItem;
	}

	public static int calcSeparateSpan(int lineWidth, String separateItem) {
		final int itemFullWidth = separateItem.length();
		final int span = lineWidth / itemFullWidth;
		return span;
	}

}

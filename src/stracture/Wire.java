package stracture;

/**アイテム(ワイヤー)を組み立てる
 *
 */
public class Wire {

	/**メインプログラム
	 * @param args
	 */
	public static void main(String[] args) {
		final String leftItemParts ="+";
		final String rightItemParts ="+";
		final String centerItemParts = "-";
		final int minWidth = 3;
		final int maxWidth = 40;
		excecute(leftItemParts,rightItemParts,centerItemParts, minWidth,maxWidth);

	}

	/**ワイヤーを作成する
	 * @param leftItemParts
	 * @param rightItemParts
	 * @param centerItemParts
	 * @param minWidth
	 * @param maxWidth
	 */
	public static void excecute(String leftItemParts, String rightItemParts, String centerItemParts,int minWidth,int maxWidth) {
		boolean isContinue = true;
		while(isContinue){
			final int width = inputWidth(minWidth, maxWidth);
			final String item = createItem(leftItemParts,rightItemParts,centerItemParts,width);
			showItem(item,width);
			isContinue = inputContinue();
		}
		System.out.println("機能を終了します。");


	}

	/**入力を継続するか判定
	 * @return
	 */
	public static boolean inputContinue() {
		System.out.println("続けて入力しますか?");
		System.out.println("1:入力 2:終了");
		int selectNum = new java.util.Scanner(System.in).nextInt();
		if(selectNum==1){
			return true;
		}else{
			return false;
		}

	}


	/**アイテムと情報を表示する
	 * @param item
	 * @param width
	 */
	public static void showItem(String item,int width) {
		System.out.println(item);
		System.out.println("長さ"+width+"m");

	}

	/**アイテムの長さをキーボード入力で設定する
	 * @param minWidth
	 * @param maxWidth
	 * @return
	 */
	public static int inputWidth(int minWidth,int maxWidth) {
		System.out.println("長さを数値で入力してください。");
		int input = new java.util.Scanner(System.in).nextInt();
		if(input <= minWidth){
			input = minWidth;
		}else if(input >= maxWidth){
			input = maxWidth;
		}
		return input;

	}
	/**テキストをアイテムに組み立てる
	 * @param leftItemParts
	 * @param rightItemParts
	 * @param centerItemParts
	 * @param width
	 * @return
	 */
	public static String createItem(String leftItemParts, String rightItemParts, String centerItemParts,int width) {
		String item = "";
		item+=leftItemParts;
		for(int i=0;i<width;i++){
			item+=centerItemParts;
		}
		item+=rightItemParts;
		return item;
	}

}

package stracture;

/**アイテム(領域)を組み立てる
 *
 */
public class Area {

	/**メインメソッド
	 * @param args
	 */
	public static void main(String[] args) {

		final String itemParts = "■";
		final int minWidth = 3;
		final int maxWidth = 40;
		final int minHeight = 3;
		final int maxHeight = 10;
		excecute(itemParts, minWidth,maxWidth,minHeight,maxHeight);

	}

	/**アイテムを作成、表示する
	 * @param itemParts
	 * @param minWidth
	 * @param maxWidth
	 * @param minHeight
	 * @param maxHeight
	 */
	public static void excecute(String itemParts, int minWidth,int maxWidth,int minHeight,int maxHeight) {
		boolean isContinue = true;
		while(isContinue){
			final int width = inputWidth(minWidth, maxWidth);
			final int height = inputHeight(minHeight, maxHeight);
			final String item = createItem(itemParts,width,height);
			showItem(item,width,height);
			isContinue = inputContinue();
		}
		System.out.println("機能を終了します。");
	}

	/**アイテムの長さをキーボード入力で取得する
	 * @param minWidth
	 * @param maxWidth
	 * @return
	 */
	public static int inputWidth(int minWidth,int maxWidth) {
		System.out.println("長さを数値で入力してください。");
		int input = new java.util.Scanner(System.in).nextInt();
		if(input <= minWidth){
			input = minWidth;
		}else if(input >= maxWidth){
			input = maxWidth;
		}
		return input;

	}

	/**アイテムの高さをキーボード入力で取得する
	 * @param minHeight
	 * @param maxHeight
	 * @return
	 */
	public static int inputHeight(int minHeight,int maxHeight) {
		System.out.println("高さを数値で入力してください。");
		int input = new java.util.Scanner(System.in).nextInt();
		if(input <= minHeight){
			input = minHeight;
		}else if(input >= maxHeight){
			input = maxHeight;
		}
		return input;

	}

	/**入力を継続するか判定
	 * @return
	 */
	public static boolean inputContinue() {
		System.out.println("続けて入力しますか?");
		System.out.println("1:入力 2:終了");
		int selectNum = new java.util.Scanner(System.in).nextInt();
		if(selectNum==1){
			return true;
		}else{
			return false;
		}

	}

	/**アイテムと情報を表示する
	 * @param item
	 * @param width
	 * @param height
	 */
	public static void showItem(String item,int width,int height) {
		System.out.println(item);
		System.out.println("長さ"+width+"m x 高さ"+height+"m");
	}


	/**テキストからアイテムを組み立てる
	 * @param itemParts
	 * @param width
	 * @param height
	 * @return
	 */
	public static String createItem(String itemParts, int width,int height) {
		String item = "";
		for(int i=0;i<height;i++){
			for (int j=0;j<width;j++){
				item+=itemParts;
			}
			item+="\n";
		}
		return item;
	}

}
